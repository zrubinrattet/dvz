;(function ( $, window, document, undefined ) {

	$(document).ready(function(){

		var MobileNav = {
			toggle : $('.mobilenavtoggle'),
			nav : $('.nav'),
			mobile : null,
			_init : function(){
				MobileNav._mobileCheck();
				MobileNav.toggle.on('click', MobileNav._clickHandler);
				$(window).on('resize load', MobileNav._resizeLoadHandler);
			},
			_mobileCheck : function(){
				if($(window).width() > 1024){
					MobileNav.mobile = false;
				}
				else{
					MobileNav.mobile = true;
				}
			},
			_clickHandler : function(){
				if(MobileNav.nav.hasClass('nav--visible') == false){
					MobileNav._open();
				}
				else{
					MobileNav._close();	
				}
			},
			_open : function(){
				MobileNav.nav.addClass('nav--visible');
				MobileNav.toggle.removeClass('fa-bars');
				MobileNav.toggle.addClass('fa-times');
			},
			_close : function(){
				MobileNav.nav.removeClass('nav--visible');
				MobileNav.toggle.removeClass('fa-times');
				MobileNav.toggle.addClass('fa-bars');
			},
			_resizeLoadHandler : function(){
				// open nav if in desktop else close
				MobileNav._mobileCheck();
				if($(window).width() > 1024){
					if(MobileNav.mobile){
						MobileNav._open();
					}
				}
				else{
					MobileNav._close();
				}
			},
		};
		MobileNav._init();

		var ScrollSpy = {
			navitems : $('.nav-itemcontainer-item, .hero-itemcontainer-item, .nav-logocontainer'),
			_init : function(){
				ScrollSpy.navitems.on('click', ScrollSpy._clickHandler);
				$(window).on('load', ScrollSpy._onLoadHandler);
			},
			_clickHandler : function(e){
				if( e != undefined && e.target.href.replace(e.target.hash, '') == window.location.href.replace(window.location.hash, '') && is_front_page ){
					e.preventDefault();
					MobileNav._close();
					if($(e.target.hash).length > 0){
						var target = e.target.hash;
					}
					else{
						var target = $('.hero');
					}
					$('body, html').animate({
						'scrollTop': MobileNav.mobile ? $(target).offset().top : $(target).offset().top - $('.navtint')[0].clientHeight,
					}, 500);
				}
			}
		};
		ScrollSpy._init();

		var DesktopNav = {
			nav : $('.nav'),
			heroItems : $('.hero-itemcontainer'),
			hero : $('.hero'),
			tint : $('.navtint'),
			desktopItems : $('.nav-desktopitems'),
			cta : $('.hero-ctacontainer-cta'),
			ctaContainer : $('.hero-ctacontainer'),
			_init : function(){
				$(window).on('load scroll', DesktopNav._scrollHandler);
				DesktopNav.cta.on('click', DesktopNav._ctaClickHandler);
			},
			_scrollHandler : function(){
				DesktopNav.ctaContainer.css('opacity', 1 - ($(window).scrollTop() / 200) );
				if(DesktopNav.nav.offset().top + DesktopNav.nav[0].clientHeight >= DesktopNav.hero.height() - DesktopNav.heroItems[0].clientHeight){
					DesktopNav.heroItems.addClass('hero-itemcontainer--sticky');
					DesktopNav.tint.addClass('navtint--on');
				}
				else{
					DesktopNav.heroItems.removeClass('hero-itemcontainer--sticky');	
					DesktopNav.tint.removeClass('navtint--on');
				}
				if(DesktopNav.nav.offset().top + DesktopNav.nav[0].clientHeight >= (DesktopNav.hero.height() / 2) - DesktopNav.heroItems[0].clientHeight){
					DesktopNav.desktopItems.addClass('nav-desktopitems--off');
				}
				else{
					DesktopNav.desktopItems.removeClass('nav-desktopitems--off');	
				}
			},
			_ctaClickHandler : function(e){
				e.preventDefault();
				$('html, body').animate({
					scrollTop : MobileNav.mobile ? $('#hire').offset().top : $('#hire').offset().top - $('.navtint')[0].clientHeight,
				});
			}
		}
		DesktopNav._init();

		var HireDrewButton = {
			button : $('.about-hirebutton'),
			_init : function(){
				HireDrewButton.button.on('click', HireDrewButton._clickHandler);
			},
			_clickHandler : function(e){
				e.preventDefault();
				$('body, html').animate({
					'scrollTop': MobileNav.mobile ? $('#hire').offset().top : $('#hire').offset().top - $('.navtint')[0].clientHeight,
				}, 500);
			},
		};
		HireDrewButton._init();

		var Testimonials = {
			grid : $('.testimonials-grid'),
			gridItem : $('.testimonials-grid-item'),
			arrows : $('.testimonials-grid-arrows img'),
			isTablet : false,
			isDesktop : false,
			setupMasonry : false,
			position: 0,
			masonry : undefined,
			_init : function(){
				$(window).on('resize load', Testimonials._resizeLoadHandler);
			},
			_resizeLoadHandler : function(){
				// deal with masonry logic
				if($(window).width() < 1025 && $(window).width() > 640){
					if(Testimonials.isTablet == false){
						Testimonials._setupMasonry();
						Testimonials.isTablet = true;	
					}
				}
				else{
					Testimonials._destroyMasonry();
					Testimonials.isTablet = false;
				}

				// deal with slider logic
				if($(window).width() > 1024){
					if(Testimonials.isDesktop == false){
						// start slider
						Testimonials.arrows.on('click', Testimonials._arrowClickHandler);
						Testimonials.isDesktop = true;	
					}
				}
				else{
					// destroy slider
					Testimonials.arrows.off('click');
					Testimonials.gridItem.removeClass('testimonials-grid-item--visible');
					$(Testimonials.gridItem[0]).addClass('testimonials-grid-item--visible');
					Testimonials.isDesktop = false;
				}
			},
			_arrowClickHandler : function(e){
				if($(e.target).hasClass('testimonials-grid-arrows-right')){
					// go right
					Testimonials.position++;
					if(Testimonials.position > Testimonials.gridItem.length - 1){
						Testimonials.position = 0;
					}
				}
				else{
					// go left
					Testimonials.position--;
					if(Testimonials.position < 0){
						Testimonials.position = Testimonials.gridItem.length - 1;
					}
				}
				Testimonials.gridItem.removeClass('testimonials-grid-item--visible');
				$(Testimonials.gridItem[Testimonials.position]).addClass('testimonials-grid-item--visible');
			},
			_setupMasonry : function(){
				if(Testimonials.isTablet == false){
					Testimonials.masonry = $(Testimonials.grid).masonry({
						itemSelector : Testimonials.gridItem.selector,
						gutter : 40,
					});
				}
			},
			_destroyMasonry : function(){
				if(Testimonials.isTablet){
					if(Testimonials.masonry != undefined){
						Testimonials.masonry.masonry('destroy');
					}
				}	
			},
		};
		Testimonials._init();

	});

})( jQuery, window, document );

