<section class="whydrew section" id="mywork">
	<div class="section-wrapper">
		<h2 class="whydrew-header sectionheader">My Work</h2>
		<div class="whydrew-wysiwyg">
			<?php echo wpautop( get_field('whydrew_wysiwyg', 'option') ); ?>
		</div>
	</div>
</section>