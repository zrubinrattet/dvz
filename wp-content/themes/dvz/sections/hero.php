<section class="hero">
	<div class="hero-mobile">
		<img src="<?php echo get_template_directory_uri(); ?>/library/img/logo.png" class="hero-mobile-logo">
		<div class="hero-mobile-skills">
			<div class="hero-mobile-skills-item">personal training</div>
			<div class="hero-mobile-skills-item">movement specialist</div>
		</div>
	</div>
	<div class="hero-itemcontainer">
		<?php
			$implicit_url_prefix = '#';
			if( !is_front_page() ){
				$implicit_url_prefix = site_url() . '/#';	
			}
		?>
		<a href="<?php echo $implicit_url_prefix; ?>mystory" class="hero-itemcontainer-item">my story</a>
		<a href="<?php echo $implicit_url_prefix; ?>mywork" class="hero-itemcontainer-item">my work</a>
		<a href="<?php echo $implicit_url_prefix; ?>testimonials" class="hero-itemcontainer-item">testimonials</a>
		<a href="<?php echo $implicit_url_prefix; ?>hire" class="hero-itemcontainer-item">work with me</a>
		<a href="<?php echo $implicit_url_prefix; ?>about" class="hero-itemcontainer-item">about</a>
	</div>
	<div class="hero-ctacontainer">
		<div class="hero-ctacontainer-copy"><?php the_field('hero_wysiwyg', 'option'); ?></div>
	</div>
	<div class="hero-videocontainer">
		<video loop muted autoplay poster="<?php echo get_field('hero-placeholder', 'option');?>" class="hero-videocontainer-video">
		    <source src="<?php echo get_field('hero-webm', 'option');?>" type="video/webm">
		    <source src="<?php echo get_field('hero-mp4', 'option');?>" type="video/mp4">
		    <source src="<?php echo get_field('hero-ogv', 'option');?>" type="video/ogg">
		</video>
	</div>
</section>