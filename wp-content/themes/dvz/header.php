<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="google-site-verification" content="gUYptT_2ZPmUcD5c680vwj1-54wU1wK9PilUy-7xhw4" />
		<meta property="og:image" content="<?php echo get_template_directory_uri() ?>/library/img/drew.jpg" />		
		<title><?php echo get_bloginfo('name');?></title>
		<?php wp_head(); ?>		

		<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	</head>
	<body <?php body_class(); ?>>
		<style type="text/css">
			/* text color */
			body *,
			.nav-cell a,
			.hero-itemcontainer-item,
			.footer-contact > a{
				color: <?php the_field('color1', 'option'); ?> !important;
			}
			section.section:nth-child(odd){
				background-color: <?php the_field('color2', 'option'); ?> !important;
			}
			section.section:nth-child(even){
				background-color: <?php the_field('color3', 'option'); ?> !important;
			}
			.hireform-submit,
			.about-hirebutton{
				background-color: <?php the_field('color4', 'option'); ?> !important;
			}
			.sectionheader::after {
				border-color: <?php the_field('color5', 'option'); ?> !important;
			}
			.navtint--on{
				background-color: <?php the_field('color6', 'option'); ?>90 !important;
			}
			.footer{
				background-color: <?php the_field('color7', 'option'); ?> !important;
			}
		</style>