<?php 

get_header();

get_template_part('partials/nav');
get_template_part('sections/hero');
get_template_part('sections/mystory');
get_template_part('sections/whydrew');
get_template_part('sections/testimonials');
get_template_part('sections/hire');
get_template_part('sections/about');

get_footer();
?>