<section class="whydrew section" id="workwithme">
	<div class="section-wrapper">
		<h2 class="whydrew-header sectionheader">Work With Me</h2>
		<div class="whydrew-wysiwyg">
			<?php echo wpautop( get_field('workwithme_wysiwyg', 'option') ); ?>
		</div>
	</div>
</section>