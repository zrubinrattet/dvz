<i class="mobilenavtoggle fa fa-bars"></i>
<nav class="nav">
	<a class="nav-logocontainer" href="<?php echo site_url(); ?>">
		<img class="nav-logocontainer-logo" src="<?php echo get_template_directory_uri(); ?>/library/img/logo.png">
	</a>
	<div class="nav-desktopitems">
		<?php
			$tagline_font_size = !empty( $tfs = get_field('tagline_font_size', 'options') ) ? strval($tfs) . 'px' : '17px';
		?>
		<?php if( !empty( $tagline1 = get_field('tagline1', 'options') ) ): ?>
			<div class="nav-desktopitems-item" style="font-size: <?php echo $tagline_font_size; ?>"><?php echo $tagline1; ?></div>
		<?php endif; ?>
		<?php if( !empty( $tagline2 = get_field('tagline2', 'options') ) ): ?>
			<div class="nav-desktopitems-item" style="font-size: <?php echo $tagline_font_size; ?>"><?php echo $tagline2; ?></div>
		<?php endif; ?>
	</div>
	<div class="nav-itemcontainer">
		<?php
			$implicit_url_prefix = '#';
			if( !is_front_page() ){
				$implicit_url_prefix = site_url() . '/#';	
			}
		?>
		<a href="<?php echo $implicit_url_prefix; ?>mystory" class="nav-itemcontainer-item">my story</a>
		<a href="<?php echo $implicit_url_prefix; ?>mywork" class="nav-itemcontainer-item">my work</a>
		<a href="<?php echo $implicit_url_prefix; ?>testimonials" class="nav-itemcontainer-item">testimonials</a>
		<a href="<?php echo $implicit_url_prefix; ?>hire" class="nav-itemcontainer-item">work with me</a>
		<a href="<?php echo $implicit_url_prefix; ?>about" class="nav-itemcontainer-item">about</a>
	</div>
	<div class="nav-socialcontainer">
		<?php if(get_field('yelp-toggle', 'option')): ?>
			<a target="_blank" href="<?php the_field('yelp-url', 'option'); ?>" class="nav-socialcontainer-item">
				<img src="<?php echo get_template_directory_uri() ?>/library/img/yelp.png" class="nav-socialcontainer-item-image">
			</a>
		<?php endif; ?>
		<a target="_blank" href="<?php the_field('instagram-url', 'option'); ?>" class="nav-socialcontainer-item">
			<img src="<?php echo get_template_directory_uri() ?>/library/img/instagram.png" class="nav-socialcontainer-item-image">
		</a>
		<a target="_blank" href="<?php the_field('linkedin-url', 'option'); ?>" class="nav-socialcontainer-item">
			<img src="<?php echo get_template_directory_uri() ?>/library/img/linkedin.png" class="nav-socialcontainer-item-image">
		</a>
	</div>
	<div class="nav-cell"><a href="tel:5106893545">510-689-3545</a></div>
</nav>
<div class="navtint"></div>