<?php
	
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Site Settings',
		'menu_title'	=> 'Site Settings',
		'menu_slug' 	=> 'general-settings',
		'capability'	=> 'read_private_posts',
		'icon_url'      => 'dashicons-admin-settings',
		'redirect'		=> false,
		'position'      => 7,
	));
}

function add_acf_fields() {
	acf_add_local_field_group(array(
		'key' => 'group_1',
		'title' => ' ',
		'fields' => array (
			array(
				'key' => 'field_8b7ahnwfe',
				'message' => '<h1>Click on a tab below to see its corresponding settings. Click the blue "Update" button in the top right to save.</h1>',
				'type' => 'message',
			),
			array(
				'key' => 'field_73b2219F8Q3',
				'label' => 'Color Settings',
				'type' => 'tab',
			),
			array(
				'key' => 'field_color1',
				'label' => 'Color 1 (font color)',
				'name' => 'color1',
				'type' => 'color_picker',
			),
			array(
				'key' => 'field_color2',
				'label' => 'Color 2 (bg 1)',
				'name' => 'color2',
				'type' => 'color_picker',
			),
			array(
				'key' => 'field_color3',
				'label' => 'Color 3 (bg 2)',
				'name' => 'color3',
				'type' => 'color_picker',
			),
			array(
				'key' => 'field_color4',
				'label' => 'Color 4 (button bg color)',
				'name' => 'color4',
				'type' => 'color_picker',
			),
			array(
				'key' => 'field_color5',
				'label' => 'Color 5 (section title line color)',
				'name' => 'color5',
				'type' => 'color_picker',
			),
			array(
				'key' => 'field_color6',
				'label' => 'Color 6 (top bar bg color)',
				'name' => 'color6',
				'type' => 'color_picker',
			),
			array(
				'key' => 'field_color7',
				'label' => 'Color 7 (bottom bar bg color)',
				'name' => 'color7',
				'type' => 'color_picker',
			),
			array(
				'key' => 'field_73b22123',
				'label' => 'Nav Settings',
				'type' => 'tab',
			),
			array(
				'key' => 'field_73n1ndysya',
				'label' => 'Instagram Link',
				'type' => 'url',
				'name' => 'instagram-url',
			),
			array(
				'key' => 'field_73n1ndys1122ya',
				'label' => 'Linkedin Link',
				'type' => 'url',
				'name' => 'linkedin-url',
			),
			array(
				'key' => 'field_nhawef812',
				'label' => 'Turn on Yelp Link?',
				'name' => 'yelp-toggle',
				'type' => 'true_false',
				'instructions' => 'Check this box if you want the Yelp icon that links to your Yelp page to show up',
			),
			array(
				'key' => 'field_abbabd882',
				'label' => 'Yelp Link',
				'name' => 'yelp-url',
				'type' => 'url',
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_nhawef812',
							'operator' => '==',
							'value' => 1,
						),
					),
				),
			),
			array(
				'key' => 'field_876646',
				'label' => 'Hero Section',
				'type' => 'tab',
			),
			array(
				'key' => 'field_hero_wysiyg',
				'label' => 'Hero WYSIWYG',
				'name' => 'hero_wysiwyg',
				'type' => 'wysiwyg',
			),
			array(
				'key' => 'field_832bbxudad',
				'message' => '<h2 style="padding: 8px 0px;"><strong>Hero Videos</strong></h2>The reason for multiple file types is for fallback purposes. Some devices and browsers can only read certain video file types.',
				'type' => 'message',
			),
			array(
				'key' => 'field_832dabbdao',
				'label' => 'Placeholder image',
				'type' => 'image',
				'return_format' => 'url',
				'name' => 'hero-placeholder',
			),
			array(
				'key' => 'field_832dao',
				'label' => 'mp4',
				'type' => 'file',
				'return_format' => 'url',
				'name' => 'hero-mp4',
			),
			array(
				'key' => 'field_832dffdao',
				'label' => 'ogv',
				'type' => 'file',
				'return_format' => 'url',
				'name' => 'hero-ogv',
			),
			array(
				'key' => 'field_aa832dao',
				'label' => 'webm',
				'type' => 'file',
				'return_format' => 'url',
				'name' => 'hero-webm',
			),
			array(
				'key' => 'field_87nvcvczdaf',
				'label' => 'My Story Section',
				'type' => 'tab',
			),
			array(
				'key' => 'field_kb1234asfada',
				'label' => 'Add Content',
				'type' => 'wysiwyg',
				'name' => 'mystory_wysiwyg'
			),
			array(
				'key' => 'field_09fajdfaif',
				'label' => 'My Work Section',
				'type' => 'tab',
			),
			array(
				'key' => 'field_kbnahewf',
				'label' => 'Add Content',
				'type' => 'wysiwyg',
				'name' => 'whydrew_wysiwyg'
			),
			array(
				'key' => 'field_7bhzhdapfd',
				'label' => 'About Section',
				'type' => 'tab',
			),
			array(
				'key' => 'field_9123fdad',
				'label' => 'About Section Copy',
				'type' => 'wysiwyg',
				'name' => 'about-wysiwyg',
			),
			array(
				'key' => 'field_nmadmfi32',
				'label' => 'Hire Drew Section',
				'type' => 'tab',
			),
			array(
				'key' => 'field_8banaedf',
				'name' => 'hiredrew-message',
				'type' => 'wysiwyg',
				'label' => 'Subheader Copy',
			),
			
			
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'general-settings',
				),
			),
		),
	));
}

add_action('acf/init', 'add_acf_fields');

?>