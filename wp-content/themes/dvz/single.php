<?php 

get_header();

get_template_part('partials/nav');
get_template_part('sections/hero');
?>
<div class="singlepost">
	<div class="singlepost-wrapper">
		<h1 class="singlepost-wrapper-title"><?php the_title(); ?></h1>
		<h3 class="singlepost-wrapper-date">Posted on: <?php echo date('m/d/Y g:ia', strtotime($post->post_date)) ?></h3>
		<div class="singlepost-wrapper-content">
			<?php echo apply_filters( 'the_content', do_shortcode($post->post_content) ); ?>
		</div>
	</div>
</div>
<?php
get_template_part('sections/hire');

get_footer();
?>