<?php
	get_header();

	get_template_part('partials/nav');
	get_template_part('sections/hero');

	$the_query = new WP_Query(array(
		'post_type' => 'post',
		'posts_per_page' => 24,
		'offset' => 0,
		'post_status' => 'publish',
	));

	if( $the_query->have_posts() ): ?>
		<div class="blogwrapper">
			<h1 class="blogwrapper-title">Blog</h1>
			<div class="blogwrapper-grid">
				<?php
					while( $the_query->have_posts() ): $the_query->the_post();?>
						<a href="<?php the_permalink(); ?>" class="blogwrapper-grid-item">
							<?php if( has_post_thumbnail() ): ?>
								<div class="blogwrapper-grid-item-img" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID, 'medium'); ?>);"></div>
							<?php endif; ?>
							<div class="blogwrapper-grid-item-text">
								<h3 class="blogwrapper-grid-item-text-title"><?php the_title(); ?></h3>
								<h4 class="blogwrapper-grid-item-text-date">Posted on: <?php echo date('m/d/Y g:ia', strtotime($post->post_date)) ?></h4>
								<div class="blogwrapper-grid-item-text-description"><?php echo wp_trim_words( get_the_content(), 20 ); ?></div>
							</div>
						</a>
				<?php
					endwhile;
				?>
			</div>
		</div>
	<?php
	endif;
	get_template_part('sections/hire');
	get_footer();
?>