<?php 


// configure style/script registration/enqueing, menu registration, after_setup_theme filter
show_admin_bar( false );

add_action( 'after_setup_theme', function(){
	add_theme_support( 'html5' );
	add_theme_support( 'post-thumbnails' );
});

add_action( 'init', function(){
	clean_head();
	register_nav_menu( 'main-nav', 'Main Navigation');
});

add_action( 'wp_enqueue_scripts', function(){
	register_styles();
	enqueue_styles();
	register_javascript();
	enqueue_javascript();		
	localize_scripts();
});

function localize_scripts(){
	@wp_localize_script( 'theme', 'is_front_page', is_front_page() ? 1 : 0 );
}

function enqueue_javascript(){
	wp_enqueue_script( 'theme' );
}
function enqueue_styles(){
	wp_enqueue_style( 'theme', '', array(), filemtime(get_template_directory() . '/build/css/build.css') );
}

function register_javascript(){
	wp_register_script( 'theme', get_template_directory_uri() . '/build/js/build.js');
}

function register_styles(){
	wp_register_style( 'theme', get_template_directory_uri() . '/build/css/build.css', array(), filemtime(get_template_directory() . '/build/css/build.css') );
}

function clean_head(){
	// removes generator tag
	remove_action( 'wp_head' , 'wp_generator' );
	// removes dns pre-fetch
	remove_action( 'wp_head', 'wp_resource_hints', 2 );
	// removes weblog client link
	remove_action( 'wp_head', 'rsd_link' );
	// removes windows live writer manifest link
	remove_action( 'wp_head', 'wlwmanifest_link');	
}

require('acf.php');

// setup editor and author admin backend
function setup_editor_admin(){
    $user = wp_get_current_user();
    $allowed_roles = array('editor');

    if ( array_intersect($allowed_roles, $user->roles ) ) {
		remove_menu_page( 'edit.php?post_type=page' );
		remove_menu_page( 'tools.php' );
		remove_menu_page( 'profile.php' );
		remove_menu_page( 'edit-comments.php' );
		remove_menu_page( 'edit.php' );
		remove_menu_page( 'wpcf7' );

		add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
    }
    if ( array_intersect( array('author'), $user->roles ) ) {
    	remove_menu_page( 'edit.php?post_type=coupon' );
    	$user->remove_cap('gform_full_access');
    }
}

add_action('admin_init','setup_editor_admin');

function remove_dashboard_widgets() {
	global $wp_meta_boxes;

	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);

}

function dashboard_redirect($url) {
        return 'wp-admin/admin.php?page=general-settings';
}
add_filter('login_redirect', 'dashboard_redirect');   

function remove_menus () {
    global $menu;
    $restricted = array(__('Dashboard'));
    //$restricted = array(__('Dashboard'), __('Posts'), __('Media'), __('Links'), __('Pages'), __('Appearance'), __('Tools'), __('Users'), __('Settings'), __('Comments'), __('Plugins'));
    end($menu);
    while(prev($menu)){
        $value = explode(' ',$menu[key($menu)][0]);
        if(in_array($value[0]!= NULL?$value[0]:'',$restricted)){unset($menu[key($menu)]);}
    }
}
add_action('admin_menu','remove_menus');














?>