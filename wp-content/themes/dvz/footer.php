	<section class="footer">
		<div class="footer-contact">
			Drew Van Zee<br/>
			<a href="https://goo.gl/maps/HJDb8tQ8wus7RGYZ6">399 Grand Ave Oakland, CA, 94610</a><br/>
			<a href="tel:5106893545">510-689-3545</a>
		</div>
		<div class="footer-text">Copyright &copy; <?php echo Date('Y'); ?> Drew Van Zee. All rights reserved.</div>
	</section>

<?php
	if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
	   ?> <script src="//localhost:35729/livereload.js"></script> <?php
	}
	?>
</body>
</html>