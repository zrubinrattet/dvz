<section class="testimonials section" style="background-image: url('<?php the_field('testimonials-bg', 'option'); ?>');" id="testimonials">
	<div class="section-wrapper">
		<h2 class="testimonials-header sectionheader">testimonials</h2>
		<div class="testimonials-widget">
			<?php
				echo do_shortcode('[reviews_rating limit="5" excerpt="500" attribution="false"]');
			?>
		</div>
	</div>
</section>