<section class="hire section" id="hire">
	<div class="section-wrapper">
		<h2 class="hire-header sectionheader">work with me</h2>
		<div class="hire-map">
			<div style="max-width:100%; height: 250px; margin-bottom: 40px;list-style:none; transition: none;overflow:hidden;"><div id="embeddedmap-display" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=drew+van+zee&key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8"></iframe></div><style>#embeddedmap-display img{max-height:none;max-width:none!important;background:none!important;}</style></div>
		</div>
		<div class="hire-subheader">
			<?php the_field('hiredrew-message', 'option'); ?>
		</div>
		<div class="hire-formcontainer">
			<?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
		</div>
	</div>
</section>